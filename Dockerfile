FROM mhart/alpine-node


RUN apk add --no-cache git
RUN npm install -g grunt-cli
RUN npm install -g http-server
RUN git clone https://github.com/gchq/CyberChef.git /app

RUN cd /app && npm install && grunt prod

WORKDIR /app/build/prod

ENTRYPOINT ["http-server", "-p", "8000"]
